This was a simple static site for testing some things with git.

It's now a Hugo site with potential for lots of flexbox stuff.

Think this site was originally uploaded directly to Netlify (maybe?).

Became a full working Hugo site from 30.12.22.

Repo on GitLab.

Hosted at: https://learnflexbox.netlify.app