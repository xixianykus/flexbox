const firstList = document.querySelector('#first-list');
const flexList = document.querySelector('#flex-list');

const displayFlexBtn = document.querySelector('#display-flex');
const flexWrapBtn = document.querySelector('#flex-wrap');
const gapBtn = document.querySelector('#gap');
const flex1Btn = document.querySelector('#flex-1');

displayFlexBtn.addEventListener('click', () => {
  firstList.classList.toggle('df');
  displayFlexBtn.classList.toggle('active');
});

flexWrapBtn.addEventListener('click', () => {
  firstList.classList.toggle('fw');
  flexWrapBtn.classList.toggle('active');
});

gapBtn.addEventListener('click', () => {
  firstList.classList.toggle('gap');
  gapBtn.classList.toggle('active');
});

flex1Btn.addEventListener('click', () => {
  firstList.classList.toggle('flex-1');
  flex1Btn.classList.toggle('active');
});
