const flexList = document.querySelector('#flex-list');

const gapBtn = document.querySelector('#gap');
const flex1Btn = document.querySelector('#flex-1');

gapBtn.addEventListener('click', () => {
  flexList.classList.toggle('gap');
  gapBtn.classList.toggle('active');
});

flex1Btn.addEventListener('click', () => {
  flexList.classList.toggle('flex-1');
  flex1Btn.classList.toggle('active');
});
